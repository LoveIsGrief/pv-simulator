"""
mobilityhouse
Copyright (C) 2021 LoveIsGrief

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import logging
import random
import signal
from typing import Generator, Tuple

import pika

from mobilityhouse.common import (
    BYTE_ORDER,
    BYTES_FOR_INT,
    DAY_IN_SECONDS,
    QUEUE_NAME,
    SAMPLE_INTERVAL,
    create_int_handler,
)

logger = logging.getLogger("meter")

MIN_WATTAGE = 0
MAX_WATTAGE = 9000


def main():
    connection = pika.BlockingConnection(
        pika.ConnectionParameters("rabbitmq", retry_delay=5, connection_attempts=3)
    )
    signal.signal(signal.SIGINT, create_int_handler(connection))
    signal.signal(signal.SIGTERM, create_int_handler(connection))

    channel = connection.channel()

    channel.queue_declare(queue=QUEUE_NAME)

    samples_to_send = int(DAY_IN_SECONDS / SAMPLE_INTERVAL)
    logger.info("Sending %s samples", samples_to_send)
    for sample_no, watts in generate_random_watts(MIN_WATTAGE, MAX_WATTAGE, samples_to_send):
        channel.basic_publish(
            exchange="", routing_key=QUEUE_NAME, body=watts.to_bytes(BYTES_FOR_INT, BYTE_ORDER)
        )
        logger.info("Sent sample %s: %s", sample_no + 1, watts)

    logger.info("All samples sent")
    connection.close()


def generate_random_watts(
    _min: int, _max: int, count: int
) -> Generator[Tuple[int, int], None, None]:
    if _min < 0:
        raise ValueError("Cannot have negative minimum wattage")
    if _min >= _max:
        raise ValueError("Min should be greater than Max")
    if count <= 0:
        raise ValueError("Count should be greater than 0")
    for i in range(count):
        yield i, random.randint(_min, _max)


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    main()
