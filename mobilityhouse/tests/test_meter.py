import unittest

from mobilityhouse.meter import generate_random_watts


class TestGenerateRandomWatts(unittest.TestCase):
    def test_good_values(self):
        _min = 0
        _max = 100
        count = 100
        watts_list = list(generate_random_watts(_min, _max, count))

        self.assertEqual(len(watts_list), count, "Unexpected length")
        self.assertTrue(
            all([_min <= watts[1] <= _max for watts in watts_list]),
            "Generated values not in acceptable range",
        )

    def assertGeneratorRaisesRegex(
        self, expected_exception, expected_regex, generator_func, *args, **kwargs
    ):
        """A helper to call assertRaisesRegex on a generator function"""

        def call_generator(*args, **kwargs):
            # Force eval of generator
            next(generator_func(*args, **kwargs))

        self.assertRaisesRegex(expected_exception, expected_regex, call_generator, *args, **kwargs)

    def test_overlapping_range(self):
        self.assertGeneratorRaisesRegex(
            ValueError, "Min should be greater than Max", generate_random_watts, 10, 1, 100
        )

    def test_no_range(self):
        self.assertGeneratorRaisesRegex(
            ValueError, "Min should be greater than Max", generate_random_watts, 1, 1, 100
        )

    def test_negative_minimum(self):
        self.assertGeneratorRaisesRegex(
            ValueError, "Cannot have negative minimum wattage", generate_random_watts, -100, 1, 100
        )

    def test_negative_count(self):
        self.assertGeneratorRaisesRegex(
            ValueError, "Count should be greater than 0", generate_random_watts, 1, 100, -200
        )


if __name__ == "__main__":
    unittest.main()
