"""
mobilityhouse
Copyright (C) 2021 LoveIsGrief

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import abc
import argparse
import csv
import datetime
import logging
import signal
from datetime import timedelta
from pathlib import Path
from typing import Tuple

import pika

from mobilityhouse.common import (
    BYTE_ORDER,
    DAY_IN_SECONDS,
    QUEUE_NAME,
    SAMPLE_INTERVAL,
    create_int_handler,
)

logger = logging.getLogger("simulator")


def main(csv_path: Path):
    connection = pika.BlockingConnection(
        pika.ConnectionParameters("rabbitmq", retry_delay=5, connection_attempts=3)
    )
    channel = connection.channel()
    signal.signal(signal.SIGINT, create_int_handler(connection))
    signal.signal(signal.SIGTERM, create_int_handler(connection))

    channel.queue_declare(queue=QUEUE_NAME)

    logger.debug("basic consume")
    handler = RMQHandler(
        SingleDaySimulator(
            start=timedelta(hours=7).total_seconds(),
            peak=timedelta(hours=13).total_seconds(),
            end=timedelta(hours=21).total_seconds(),
            max_output=1000,
        ),
        csv_path,
    )
    channel.basic_consume(queue=QUEUE_NAME, auto_ack=True, on_message_callback=handler)

    logger.info("Waiting for messages. To exit press CTRL+C")
    channel.start_consuming()


class RMQHandler:
    """RabbitMQ handler that handles the messages from the meter"""

    CSV_FIELDS = [
        "timestamp",
        "meter",
        "pv",
        "sum",
    ]

    def __init__(self, simulator: "Simulator", filepath: Path):
        self.simulator = simulator
        self.sample_count = 0
        self.filepath = filepath

    def __call__(self, ch, method, properties, body: bytes):
        """Collect meter samples, count and sum them with simulated PV unit and append to CSV"""
        value = int.from_bytes(body, BYTE_ORDER)
        self.sample_count += 1

        pv = self.simulator.simulate_pv_value(self.sample_count)
        # Necessary to create a timestamp starting a midnight (hence the "-1")
        tod = datetime.datetime.fromtimestamp(0) + timedelta(
            seconds=(self.sample_count - 1) * SAMPLE_INTERVAL
        )
        self.write(value, pv, tod)
        logger.info("Sample %s - TOD %s: %r + %r", self.sample_count, tod.time(), value, pv)

    def write(self, meter_value: int, pv_value: int, tod: datetime.datetime):
        """Write out the received and simulated values to a CSV"""
        is_new = not self.filepath.is_file()

        with open(self.filepath, mode="a") as file:
            writer = csv.DictWriter(file, fieldnames=self.CSV_FIELDS)
            if is_new:
                writer.writeheader()
            writer.writerow(
                {
                    "timestamp": tod.timestamp(),
                    "meter": meter_value,
                    "pv": pv_value,
                    "sum": meter_value + pv_value,
                }
            )


class Simulator(abc.ABC):
    def __init__(self, max_output: int = 100, sample_interval=SAMPLE_INTERVAL):
        """
        :param max_output: Max wattage of this PV unit
        """
        self.max_output = max_output
        self.sample_interval = sample_interval

    def simulate_pv_value(self, sample_no) -> int:
        """
        Simulates a PV unit

        :return: value in watts >= 0
        """

        value = self._simulate_pv_value(sample_no)
        # force value between 0 and max_output
        return min(max(value, 0), self.max_output)

    @abc.abstractmethod
    def _simulate_pv_value(self, sample_no) -> int:
        """Calculates the simulated wattage"""
        raise NotImplementedError()

    def sample2seconds(self, sample_no):
        return ((sample_no - 1) * self.sample_interval) % DAY_IN_SECONDS


class SingleDaySimulator(Simulator):
    """Simulate a PV unit on a good day when the sun hits the zenith"""

    def __init__(
        self, start: int, peak: int, end: int, max_output: int, sample_interval=SAMPLE_INTERVAL
    ):
        """
        :param start: time of sunrise in seconds
        :param end: time of sunset in seconds
        :param peak: time of zenith in seconds
        :param max_output: max watts of the PV unit
        :param sample_interval:
        """
        super().__init__(max_output, sample_interval)
        self.start = start
        self.end = end
        self.peak = peak

        if not (start < peak < end):
            raise ValueError("Start, peak, and end must progress positively")
        if max_output <= 0:
            raise ValueError("Max output must be above 0")

        self.a, self.b, self.c = self.solve_polynomial(start, end, peak, max_output)

    @staticmethod
    def solve_polynomial(
        start: int, end: int, peak: int, amplitude: int
    ) -> Tuple[float, float, float]:
        """
        Calculate a,b,c of f(x) = axª + bx + c

        Taken from https://stackoverflow.com/a/717791
        """
        # Make 3 points
        x1, y1 = start, 0
        x2, y2 = peak, amplitude
        x3, y3 = end, 0

        # Calc polynomials from points
        denom = (x1 - x2) * (x1 - x3) * (x2 - x3)
        a = (x3 * (y2 - y1) + x2 * (y1 - y3) + x1 * (y3 - y2)) / denom
        b = ((x3 * x3) * (y1 - y2) + (x2 * x2) * (y3 - y1) + (x1 * x1) * (y2 - y3)) / denom
        c = (x2 * x3 * (x2 - x3) * y1 + x3 * x1 * (x3 - x1) * y2 + x1 * x2 * (x1 - x2) * y3) / denom

        return a, b, c

    def _simulate_pv_value(self, sample_no) -> int:
        """
        Using a polynomial axª + bx + c we simulate the output of a PV unit

        It can happen that a value is above the max output,
         because the polynomial isn't a perfect fit.
        That should be handled outside of this function
        """
        seconds = self.sample2seconds(sample_no)
        return int((self.a * seconds * seconds) + (self.b * seconds) + self.c)


class FileOrMissingPathArg:
    """
    argparse argument type that should be a path to a file or something that doesn't exist yet

    It assumes the parents exist though and that the process has access
    """

    def __call__(self, arg: str) -> Path:
        path = Path(arg)
        if not (path.is_file() or not path.exists()):
            raise argparse.ArgumentTypeError("Must be a file or shouldn't exist")
        return path


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    parser = argparse.ArgumentParser(description="Solar panel output simulator")
    parser.add_argument(
        "csv_path",
        help="Where to store the simulated values. "
        "Either an existing file or a path to a new one in an existing folder. "
        "An existing path file will be appended to!",
        type=FileOrMissingPathArg(),
    )
    args = parser.parse_args()
    main(args.csv_path)
