"""
mobilityhouse
Copyright (C) 2021 LoveIsGrief

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import logging
import sys
from datetime import timedelta

import pika

logger = logging.getLogger(__name__)

DAY_IN_SECONDS = timedelta(days=1).total_seconds()
SAMPLE_INTERVAL = 5 * 60  # 5 minutes. Do meters have a higher sampling rate?
QUEUE_NAME = "mobilityhouse"

BYTES_FOR_INT = 8  # 64 bits / 8 --> bytes
BYTE_ORDER = sys.byteorder


def create_int_handler(connection: pika.BlockingConnection):
    def handle_int(_signal, _frame):
        logger.info("Exiting...")
        connection.close()

    return handle_int
