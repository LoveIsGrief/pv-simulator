FROM python:3.9-slim as base

WORKDIR /app
COPY pyproject.toml .

# Install poetry
RUN pip install poetry \
    # Don't let poetry create virtualenvs - saves disk size
    && poetry config virtualenvs.create false \
    && poetry install --no-dev

FROM base as dev

RUN poetry install

FROM base as prod
COPY . .

# Create user with ID that hopefully doesn't match a user on the host
# Should probably be run in a different user namespace too
RUN useradd --no-create-home --uid 2989 app
USER app
