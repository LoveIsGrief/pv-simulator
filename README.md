# Photovoltaic simulator

A simple simulator for a single PV unit connected to a meter.
It resembles a real world situation where a PV unit is mounted on the roof
of a house and feeds into the electrical grid of the house.

```
                                                         +--------------+
+---------+        +------------+     +-------------+    |+------------+|
|         |        |            |     |             |    ||            ||
|  Meter  +-------->  RabbitMQ  +----->  Simulator  +---->|  Output    ||
|         |        |            |     |             |    ||            ||
+---------+        +------------+     +-------------+    |+------------+|
                                                         +--------------+
```

# Specs

The meter generates random values between 0 - 9000 (watts).
They are generated at a sample interval of 5 minutes for a total of 24 hours
 starting at midnight.

These samples are passed through RabbitMQ to a PV simulator.
The sample count is used to calculate the time of the day and a watt value for a PV unit
 at that time.

All values are written out to a CSV file by the simulator:

 - POSIX timestamp (starts from epoch, so it's just the seconds past epoch)
 - meter wattage
 - PV unit wattage
 - sum of wattages

# Running

To run the project you will require [docker-compose] and [docker].

Once both are installed and setup, you can run the project with 

```shell
docker-compose up
```

The images will be built, the services started and a `values.csv` will be output
 in the project's root.

**Caveat**

The meter service can be called multiple times even though it's not very useful,
 as the simulator will start the day at midnight / 0 seconds after epoch again.
The `values.csv` will thus have duplicate rows.

# Development

There are two ways to develop the project, but they both need docker to run `rabbitmq`.

The code is formatted using [black].

## In Docker

Make your modifications and run the service that was modified e.g

```shell
# For a single run
docker-compose run --rm meter
docker-compose run --rm simulator

# For a service that runs in the background and whose logs you can look at later
docker-compose up -d --force-recreate meter
docker-compose up -d --force-recreate simulator

# Check logs - will output logs across service restarts
docker-compose logs meter
# Follow logs (without seeing the universe's creation)
docker-compose logs --tail 10 --follow meter
docker-compose logs --tail 10 --follow simulator
```

### Tests

Everything is run with docker-compose and we just abuse one of the services
 to run a custom python command

```shell
docker-compose run --rm meter python -m unittest
```

### Formatting

If your user doesn't have the UID `1000`, you will have to modify the services in
[`docker-compose.yml`](./docker-compose.yml) to use that ID.
You can then abuse the `meter` or `simulator` service to run

```shell
docker-compose run --rm meter black mobilityhouse/
```

## Outside containers

If you wish, you can run the project in a Python 3.9 environment locally
(it should work find with python 3.6 too, but that hasn't been tested).

### Dependency installation

The project uses [poetry] (which in turn depends on `pip`) for managing python deps.

```shell
pip install poetry

# Optionally disable virtualenvs if you're already in one
# poetry config virtualenvs.create false

poetry install
```

### Services

Modify `mobilityhouse/meter.py` and `mobilityhouse/simulator.py` to use
 `localhost` instead of `rabbitmq`.
You can also modify your hosts file to have localhost be known as `rabbitmq` too.
Example:

```
127.0.0.1   localhost rabbitmq
```

Then start up the `rabbitmq` service 
```shell
docker-compose up -d rabbitmq
```

And finally start the meter or the simulator
```shell
python -m mobilityhouse.meter
python -m mobilityhouse.simulator
```

### Tests

We use python's good ol' trusty [unittest].

```shell
python -m unittest
```

### Formatting

```shell
black mobilityhouse/
```

<!-- ------------------------------ -->

[black]: https://black.readthedocs.io/en/stable/index.html
[docker-compose]: https://docs.docker.com/compose/
[docker]: https://docs.docker.com/engine/
[unittest]: https://docs.python.org/3/library/unittest.html
[poetry]: https://python-poetry.org/docs/
